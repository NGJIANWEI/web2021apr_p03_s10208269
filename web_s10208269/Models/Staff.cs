﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace web_s10208269.Models
{
    public class Staff
    {
        [Display(Name = "ID")]
        public int StaffId { get; set; }
        
        [StringLength(maximumLength:50, ErrorMessage = "Cannot exceed 50 characters")]
        [Required]
        public string Name { get; set; }

        public char Gender { get; set; }

        [Display(Name = "Date of Birth")]
        [DataType(DataType.Date)]
        public DateTime? DOB { get; set; }

        public string Nationality { get; set; }


        [Display(Name = "Email Address")]
        [EmailAddress]
        [ValidateEmailExists]
        public string Email { get; set; }

        
        [Display(Name = "Monthly Salary (SGD)")]
        [DisplayFormat(DataFormatString = "{0:#,##0.00}")]
        [Range(1.00, 10000.00)]
        public decimal Salary { get; set; }


        [Display(Name = "Full-Time Staff")]
        public bool IsFullTime { get; set; }
        [Display(Name = "Branch")]
        public int? BranchNo { get; set; }


    }
}
