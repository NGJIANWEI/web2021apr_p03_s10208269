﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace web_s10208269.Models
{
    public class Vote
    {
        public string Justification { get; set; }
        public int BookId { get; set; }
    }
}
