﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using web_s10208269.Models;
using Microsoft.AspNetCore.Authentication.Cookies;
using Google.Apis.Auth.OAuth2;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Google.Apis.Auth;
using static Google.Apis.Auth.GoogleJsonWebSignature;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication;

namespace web_s10208269.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [HttpPost]
        public ActionResult StaffLogin(IFormCollection formData)
        {
            // Read inputs from textboxes
            // Email address converted to lowercase
            string loginID = formData["txtLoginID"].ToString().ToLower();
            string password = formData["txtPassword"].ToString();
            string loginDate = DateTime.Now.ToString("dd-MMM-yy hh:mm:ss tt");                   
            if (loginID == "abc@npbook.com" && password == "pass1234")
            {
                HttpContext.Session.SetString("LoginID", loginID);
                HttpContext.Session.SetString("Role", "Staff");
                HttpContext.Session.SetString("LoginTime", loginDate);
                // Redirect user to the "StaffMain" view through an action
                return RedirectToAction("StaffMain");
   
            }
            else
            {
                TempData["Message"] = "Invalid Login Credential!";
                // Redirect user back to the index view through an action
                return RedirectToAction("Index");
            }
        }

        [Authorize]
        public async Task<ActionResult> StudentLogin()
        {
            // The user is already authenticated, so this call won't
            // trigger login, but it allows us to access token related values.
            AuthenticateResult auth = await HttpContext.AuthenticateAsync();
            string idToken = auth.Properties.GetTokenValue(
             OpenIdConnectParameterNames.IdToken);
            try
            {
                // Verify the current user logging in with Google server
                // if the ID is invalid, an exception is thrown
                Payload currentUser = await
                GoogleJsonWebSignature.ValidateAsync(idToken);
                string userName = currentUser.Name;
                string eMail = currentUser.Email;
                HttpContext.Session.SetString("LoginID", userName + " / "
                + eMail);
                HttpContext.Session.SetString("Role", "Student");
                HttpContext.Session.SetString("LoggedInTime",
                 DateTime.Now.ToString());
                return RedirectToAction("Index", "Book");
            }
            catch (Exception e)
            {
                // Token ID is may be tempered with, force user to logout
                return RedirectToAction("LogOut");
            }
        }

        public ActionResult StaffMain()
        {
            if ((HttpContext.Session.GetString("Role") == null) || (HttpContext.Session.GetString("Role") != "Staff"))
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        public async Task<ActionResult> LogOut()
        {
            /**string logoutDate = DateTime.Now.ToString("dd-MMM-yy hh:mm:ss tt");
            //var diff = DateTime.Now.Subtract(loginDate);
            DateTime loginDate = DateTime.Parse(HttpContext.Session.GetString("LoginTime"));
            TimeSpan timeSpan = (DateTime.Now - loginDate);
            var sessionDuration = timeSpan.TotalSeconds;
            TempData["LogOutMessage"] = "You have logged in for " + sessionDuration + " seconds.";
            **/
            // Clear authentication cookie
            await HttpContext.SignOutAsync(
            CookieAuthenticationDefaults.AuthenticationScheme);

            HttpContext.Session.Clear();
            return RedirectToAction("Index");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
