﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using web_s10208269.Models;

namespace web_s10208269.Controllers
{
    public class FineController : Controller
    {
        public ActionResult Calculate()
        {
            ViewData["ShowResult"] = false;

            Fine fine = new Fine();
            fine.DueDate = DateTime.Today;
            fine.FineRate = 0.50;

            return View(fine);
        }

        [HttpPost]
        public ActionResult Calculate(Fine fine)
        {
            if ((HttpContext.Session.GetString("Role") == null) || (HttpContext.Session.GetString("Role") != "Staff"))
            {
                return RedirectToAction("Index", "Home");
            }

            if(!ModelState.IsValid)
            {
                return View(fine);
            }

            double fineTotal = 0.0;
            string fineBreakdown = "";

            for(int count = 1; count <= fine.NumBooksOverdue; count++)
            {
                double fineForEachBook = count * fine.FineRate * fine.NumBooksOverdue;
                fineTotal += fineForEachBook;
                fineBreakdown += "Overdue cost for Book " + count + " = $" + fineForEachBook.ToString("#,##0.00") + "<br/>";
            }
            fine.FineAmt = fineTotal;

            ViewData["ShowResult"] = true;
            ViewData["FineBreakDown"] = fineBreakdown;

            return View(fine);
        }
    }
}
